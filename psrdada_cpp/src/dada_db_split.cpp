#ifdef PSRDADA_CPP_DADA_DB_SPLIT

namespace psrdada_cpp
{

DbSplit::DbSplit(std::vector<key_t> out_keys, MultiLog& log, std::size_t chunk_size)
: _chunk_size(chunk_size)
{
  for(uint i = 0; i < out_keys.size(); i++)
  {
    _writer.push_back(new DadaWriteClient(out_keys[i], log));
  }
  _dsize = _writer[0]->data_buffer_size();
  _hsize = _writer[0]->header_buffer_size();
  _dblock = _writer[0]->data_buffer_count();
  _hblock = _writer[0]->header_buffer_count();
  _n_writer = _writer.size();
  _input_buffer_sz = _dsize * _n_writer;
  // Proof buffer sizes
  for(uint i = 0; i < _n_writer; i++)
  {
    if(_writer[i]->data_buffer_size() != _dsize){
  		BOOST_LOG_TRIVIAL(error) << "DbSplit Error: buffer with key " << _writer[i]->id()
        << "with unexpected size. Got " <<  _writer[i]->data_buffer_size()
        << " bytes, expected " << _dsize;
      exit(1);
    }
    if(_writer[i]->header_buffer_size() != _hsize){
  		BOOST_LOG_TRIVIAL(error) << "DbSplit Error: buffer with key " << _writer[i]->id()
        << "with unexpected header size. Got " <<  _writer[i]->header_buffer_size()
        << " bytes, expected " << _hsize;
      exit(1);
    }
  }
  if(_chunk_size){
    if(_dsize % _chunk_size || _chunk_size > _dsize){
      BOOST_LOG_TRIVIAL(error) << "Chunk size must be smaller than buffer size and buffer size must be multiple of chunk size";
      exit(1);
    }
  }
  (_chunk_size == 0)?_writes_per_slot=1:_writes_per_slot = _dsize / _chunk_size;
}

DbSplit::~DbSplit()
{

}

void DbSplit::init(RawBytes &header_block)
{

  for(uint i = 0; i < _n_writer; i++)
  {
    auto &header = _writer[i]->header_stream().next();
    memcpy(header.ptr(), header_block.ptr(), header_block.used_bytes());
    header.used_bytes(header_block.used_bytes());
    _writer[i]->header_stream().release();
  }
}


bool DbSplit::operator()(RawBytes &dada_block)
{
  if(dada_block.used_bytes() > _input_buffer_sz){
    BOOST_LOG_TRIVIAL(error) << "Unexpected buffer size - Got "
      << dada_block.used_bytes() << " bytes, but expected "
      << _input_buffer_sz << " bytes";
      return true;
  }
  // Acquire all out buffers and fill them

  for(uint i = 0; i < _n_writer; i++){
    auto &slot = _writer[i]->data_stream().next();
    if(_chunk_size){
      for(uint j = 0; j < _writes_per_slot; j++){
        memcpy(slot.ptr(j*_chunk_size), dada_block.ptr((j * _n_writer + i) * _chunk_size), _chunk_size);
      }
    }
    else{
      memcpy(slot.ptr(), dada_block.ptr(i * _dsize), _dsize);
    }
    slot.used_bytes(_dsize);
  }
  // As soon as all out buffers are full release them
  for(uint i = 0; i < _n_writer; i++){
    BOOST_LOG_TRIVIAL(info) << "Releasing writer " << i;
    _writer[i]->data_stream().release();
  }
  return false;
}

}


#endif
