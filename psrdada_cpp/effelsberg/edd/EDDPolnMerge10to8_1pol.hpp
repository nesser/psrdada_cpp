#ifndef PSRDADA_CPP_EFFELSBERG_EDD_EDDPOLNMERGE10TO8_1POL_HPP
#define PSRDADA_CPP_EFFELSBERG_EDD_EDDPOLNMERGE10TO8_1POL_HPP
#define HEAP_SIZE_10BIT 5120
#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/common.hpp"
#include <vector>

namespace psrdada_cpp {
namespace effelsberg {
namespace edd {

class EDDPolnMerge10to8_1pol
{
public:
    EDDPolnMerge10to8_1pol(std::size_t nsamps_per_heap, std::size_t npol, int nthreads, DadaWriteClient& writer);
    ~EDDPolnMerge10to8_1pol();

    /**
     * @brief      A callback to be called on connection
     *             to a ring buffer.
     *
     * @detail     The first available header block in the
     *             in the ring buffer is provided as an argument.
     *             It is here that header parameters could be read
     *             if desired.
     *
     * @param      block  A RawBytes object wrapping a DADA header buffer
     */
    void init(RawBytes& block);

    /**
     * @brief      A callback to be called on acqusition of a new
     *             data block.
     *
     * @param      block  A RawBytes object wrapping a DADA data buffer
     */
    bool operator()(RawBytes& block);

private:
    std::size_t _nsamps_per_heap;
    std::size_t _npol;
    int _nthreads;  	
    DadaWriteClient& _writer;
};

} // edd
} // effelsberg
} // psrdada_cpp

#endif //PSRDADA_CPP_EFFELSBERG_EDD_EDDPOLNMERGE10TO8_1POL_HPP
