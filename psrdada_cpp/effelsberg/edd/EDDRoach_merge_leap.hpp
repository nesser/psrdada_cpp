#ifndef PSRDADA_CPP_EFFELSBERG_EDD_EDDROACH_MERGE_LEAP_HPP
#define PSRDADA_CPP_EFFELSBERG_EDD_EDDROACH_MERGE_LEAP_HPP
#define HEAP_SIZE 32000
#include "psrdada_cpp/dada_write_client.hpp"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/common.hpp"
#include <vector>

namespace psrdada_cpp {
namespace effelsberg {
namespace edd {

class EDDRoach_merge_leap
{
public:
    EDDRoach_merge_leap(std::size_t nchunck, int nthreads, DadaWriteClient& writer);
    ~EDDRoach_merge_leap();

    /**
     * @brief      A callback to be called on connection
     *             to a ring buffer.
     *
     * @detail     The first available header block in the
     *             in the ring buffer is provided as an argument.
     *             It is here that header parameters could be read
     *             if desired.
     *
     * @param      block  A RawBytes object wrapping a DADA header buffer
     */
    void init(RawBytes& block);

    /**
     * @brief      A callback to be called on acqusition of a new
     *             data block.
     *
     * @param      block  A RawBytes object wrapping a DADA data buffer
     */
    bool operator()(RawBytes& block);

private:
    std::size_t _nchunck;
    int _nthreads;
    DadaWriteClient& _writer;
};

} // edd
} // effelsberg
} // psrdada_cpp

#endif //PSRDADA_CPP_EFFELSBERG_EDD_EDDROACH_MERGE_LEAP_HPP
