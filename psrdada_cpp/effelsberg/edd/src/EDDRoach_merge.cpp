#include "psrdada_cpp/effelsberg/edd/EDDRoach_merge.hpp"
#include "ascii_header.h"
#include <immintrin.h>
#include <time.h>
#include <iomanip>
#include <cmath>

namespace psrdada_cpp {
namespace effelsberg {
namespace edd {

EDDRoach_merge::EDDRoach_merge(std::size_t nchunck, int nthreads, DadaWriteClient& writer)
	: _nchunck(nchunck)
    , _nthreads(nthreads)
	, _writer(writer)
{
}

EDDRoach_merge::~EDDRoach_merge()
{
}

void EDDRoach_merge::init(RawBytes& block)
{
	RawBytes& oblock = _writer.header_stream().next();
	if (block.used_bytes() > oblock.total_bytes())
	{
		_writer.header_stream().release();
		throw std::runtime_error("Output DADA buffer does not have enough space for header");
	}
	std::memcpy(oblock.ptr(), block.ptr(), block.used_bytes());
    char buffer[1024];
    ascii_header_get(block.ptr(), "SAMPLE_CLOCK_START", "%s", buffer);
    std::size_t sample_clock_start = std::strtoul(buffer, NULL, 0);
    ascii_header_get(block.ptr(), "CLOCK_SAMPLE", "%s", buffer);
    long double sample_clock = std::strtold(buffer, NULL);
    ascii_header_get(block.ptr(), "SYNC_TIME", "%s", buffer);
    long double sync_time = std::strtold(buffer, NULL);
    long double unix_time = sync_time + (sample_clock_start / sample_clock);
    long double mjd_time = (unix_time / 86400.0 ) + 40587;
    std::ostringstream mjd_start;
    mjd_start << std::fixed;
    mjd_start << std::setprecision(12);
    mjd_start << mjd_time;
    ascii_header_set(oblock.ptr(), "MJD_START", "%s", mjd_start.str().c_str());
    ascii_header_set(oblock.ptr(), "UNIX_TIME", "%Lf", unix_time);
	oblock.used_bytes(oblock.total_bytes());
	_writer.header_stream().release();
}

bool EDDRoach_merge::operator()(RawBytes& block)
{
	BOOST_LOG_TRIVIAL(info) << "nchucnk " << _nchunck << "\n";
	RawBytes& oblock = _writer.data_stream().next();
	std::size_t bytes_per_chunk = BYTES_PER_CHUNK;
	std::size_t heap_size = HEAP_SIZE;
	std::size_t nbands = _nchunck;
	#pragma omp parallel for num_threads(_nthreads)
	for (std::size_t xx = 0; xx < block.used_bytes() / _nchunck / heap_size; xx++)
	{
		std::vector<char*> ptrs(_nchunck);
		for (std::size_t ii = 0; ii < _nchunck; ++ii)
		{
			ptrs[ii] = block.ptr() + xx * nbands * heap_size + ii * heap_size;
		}
		const char *target = oblock.ptr() + xx * nbands * heap_size;

		for (std::size_t yy = 0; yy < heap_size / bytes_per_chunk; yy++)
		{
			for (std::size_t ii = 0; ii < _nchunck; ++ii)
			{
				std::memcpy((void*)target, (void*)ptrs[ii], bytes_per_chunk);
				ptrs[ii] += bytes_per_chunk;
				target += bytes_per_chunk;
			}
		}
	}

	//std::memcpy(oblock.ptr(), block.ptr(), block.used_bytes());
	oblock.used_bytes(block.used_bytes());
	_writer.data_stream().release();
	return false;
}
}//edd
}//effelsberg
}//psrdada_cpp

