#include "psrdada_cpp/effelsberg/edd/dada_disk_sink_leap.hpp"
#include "ascii_header.h"
#include <chrono>
#include <iostream>
#include <iomanip>
using namespace std;
using namespace std::chrono;
namespace psrdada_cpp {
namespace effelsberg {
namespace edd {
DiskSinkLeap::DiskSinkLeap(std::string prefix, int nchan)
  : _prefix(prefix)
  , _counter(0)
  , _output_streams(nchan)
  , _nchan(nchan)
{
}
DiskSinkLeap::~DiskSinkLeap()
{
}
void DiskSinkLeap::init(RawBytes& block)
{
  for (auto& of : _output_streams) {
    if (of.is_open()) {
      of.close();
    }
  }
  std::memcpy(&_header, block.ptr(), block.used_bytes());
  ascii_header_set(_header, "NCHAN", "%s", "1");
  ascii_header_set(_header, "BW", "%s", "16");
  ascii_header_get(_header, "UTC_START", "%s", _start_time);
  BOOST_LOG_TRIVIAL(debug) << "UTC_START = " << _start_time;
  char buffer[1024];
  ascii_header_get(block.ptr(), "SAMPLE_CLOCK_START", "%s", buffer);
  std::size_t sample_clock_start = std::strtoul(buffer, NULL, 0);
  ascii_header_get(block.ptr(), "CLOCK_SAMPLE", "%s", buffer);
  long double sample_clock = std::strtold(buffer, NULL);
  ascii_header_get(block.ptr(), "SYNC_TIME", "%s", buffer);
  long double sync_time = std::strtold(buffer, NULL);
  long double unix_time = sync_time + (sample_clock_start / sample_clock);
  long double mjd_time = (unix_time / 86400.0 ) + 40587;
  std::ostringstream mjd_start;
  mjd_start << std::fixed;
  mjd_start << std::setprecision(12);
  mjd_start << mjd_time;
  ascii_header_set(_header, "MJD_START", "%s", mjd_start.str().c_str());
  ascii_header_set(_header, "UNIX_TIME", "%Lf", unix_time);
}
bool DiskSinkLeap::operator()(RawBytes& block)
{
  for (auto& of : _output_streams) {
    if (of.is_open()) {
      of.close();
    }
  }
  std::size_t heap_size = HEAP_SIZE;
  int fstart = 1340;
  std::string date;
  std::stringstream ss;
  ss << _start_time;
  date = ss.str();
  std::string str2("-");
  date.replace(date.find(str2),str2.length(),"");
  date.replace(date.find(str2),str2.length(),"");
  std::string filename;
  std::stringstream xx;
  xx << _start_time;
  filename = xx.str();
  std::size_t nheap_groups = block.used_bytes() / heap_size / _nchan;
  _transpose.resize(block.used_bytes());
  #pragma omp parallel for num_threads(_nchan)
  for (std::size_t ii = 0; ii < _nchan; ++ii) {
    #pragma omp parallel for num_threads(_nchan)
    for (std::size_t jj = 0; jj < nheap_groups; ++jj) {
      std::size_t in_index = ii * heap_size + jj * heap_size * _nchan;         //TFTP
      std::size_t out_index = ii * heap_size * nheap_groups + jj * heap_size; //FTTP
      std::memcpy(&_transpose[out_index], block.ptr() + in_index, heap_size);
    }
  }
  #pragma omp parallel for num_threads(_nchan)
  for (std::size_t ii = 0; ii < _nchan; ++ii) {
    std::size_t index = ii * heap_size * nheap_groups;
    char _loop_header[HEADER_SIZE];
    std::memcpy(&_loop_header, &_header, HEADER_SIZE);
    ascii_header_set(_loop_header, "FREQ", "%d", fstart + 16 * ii);
    ascii_header_set(_loop_header, "OBS_OFFSET", "%ld", _counter);
    ascii_header_set(_loop_header, "FILE_SIZE", "%ld", 640000000);
    std::stringstream fname;
    fname << date.substr(0, 8) <<"_EFF_"<< fstart + 16 * ii <<"/"<< filename.substr(0,19) << "_" << std::setw(16) << std::setfill('0') << _counter << ".000000.dada";
    BOOST_LOG_TRIVIAL(debug) << "filename" << fname.str();
    _output_streams[ii].open(fname.str().c_str(), std::ios::out | std::ios::app | std::ios::binary);
    _output_streams[ii].write((char*) _loop_header, HEADER_SIZE);
    _output_streams[ii].write(&_transpose[index], heap_size * nheap_groups);
  }
  _counter += heap_size * nheap_groups;
  return false;
}
}//edd
}//effelsberg
} //namespace psrdada_cpp
