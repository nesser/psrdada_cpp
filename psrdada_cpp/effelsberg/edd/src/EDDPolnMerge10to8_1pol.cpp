#include "psrdada_cpp/effelsberg/edd/EDDPolnMerge10to8_1pol.hpp"
#include "ascii_header.h"
#include <immintrin.h>
#include <time.h>
#include <iomanip>
#include <cmath>

namespace psrdada_cpp {
namespace effelsberg {
namespace edd {

uint64_t interleave(uint32_t x, uint32_t y) {
    __m128i xvec = _mm_cvtsi32_si128(x);
    __m128i yvec = _mm_cvtsi32_si128(y);
    __m128i interleaved = _mm_unpacklo_epi8(yvec, xvec);
    return _mm_cvtsi128_si64(interleaved);
}

uint64_t *unpack5(uint64_t *qword, uint8_t *out)
{
    uint64_t val, rest;
    val = be64toh(*qword);
    //printf("0x%016lX\n",val);
    qword++;
    out[0] = (int8_t)(((int64_t)(( 0xFFC0000000000000 & val) <<  0) >> 54) >> 2);
    out[1] = (int8_t)(((int64_t)(( 0x003FF00000000000 & val) << 10) >> 54) >> 2);
    out[2] = (int8_t)(((int64_t)(( 0x00000FFC00000000 & val) << 20) >> 54) >> 2);
    out[3] = (int8_t)(((int64_t)(( 0x00000003FF000000 & val) << 30) >> 54) >> 2);
    out[4] = (int8_t)(((int64_t)(( 0x0000000000FFC000 & val) << 40) >> 54) >> 2);
    out[5] = (int8_t)(((int64_t)(( 0x0000000000003FF0 & val) << 50) >> 54) >> 2);
    rest =          ( 0x000000000000000F & val) << 60; // 4 bits rest.
    // 2nd:
    val = be64toh(*qword);
    //printf("0x%016lX\n",val);
    qword++;
    out[6] = (int8_t)(((int64_t)(((0xFC00000000000000 & val) >> 4) | rest) >> 54) >> 2);
    out[7] = (int8_t)(((int64_t)(( 0x03FF000000000000 & val) <<  6) >> 54) >> 2);
    out[8] = (int8_t)(((int64_t)(( 0x0000FFC000000000 & val) << 16) >> 54) >> 2);
    out[9] = (int8_t)(((int64_t)(( 0x0000003FF0000000 & val) << 26) >> 54) >> 2);
    out[10] = (int8_t)(((int64_t)(( 0x000000000FFC0000 & val) << 36) >> 54) >> 2);
    out[11] = (int8_t)(((int64_t)(( 0x000000000003FF00 & val) << 46) >> 54) >> 2);
    rest =          ( 0x00000000000000FF & val) << 56; // 8 bits rest.
    // 3rd:
    val = be64toh(*qword);
    //printf("0x%016lX\n",val);
    qword++;
    out[12] = (int8_t)(((int64_t)(((0xC000000000000000 & val) >> 8) | rest) >> 54) >> 2);
    out[13] = (int8_t)(((int64_t)(( 0x3FF0000000000000 & val) <<  2) >> 54) >> 2);
    out[14] = (int8_t)(((int64_t)(( 0x000FFC0000000000 & val) << 12) >> 54) >> 2);
    out[15] = (int8_t)(((int64_t)(( 0x000003FF00000000 & val) << 22) >> 54) >> 2);
    out[16] = (int8_t)(((int64_t)(( 0x00000000FFC00000 & val) << 32) >> 54) >> 2);
    out[17] = (int8_t)(((int64_t)(( 0x00000000003FF000 & val) << 42) >> 54) >> 2);
    out[18] = (int8_t)(((int64_t)(( 0x0000000000000FFC & val) << 52) >> 54) >> 2);
    rest =          ( 0x0000000000000003 & val) << 62; // 2 bits rest.
    // 4th:
    val = be64toh(*qword);
    //printf("0x%016lX\n",val);
    qword++;
    out[19] = (int8_t)(((int64_t)(((0xFF00000000000000 & val) >> 2) | rest) >> 54) >> 2);
    out[20] = (int8_t)(((int64_t)(( 0x00FFC00000000000 & val) <<  8) >> 54) >> 2);
    out[21] = (int8_t)(((int64_t)(( 0x00003FF000000000 & val) << 18) >> 54) >> 2);
    out[22] = (int8_t)(((int64_t)(( 0x0000000FFC000000 & val) << 28) >> 54) >> 2);
    out[23] = (int8_t)(((int64_t)(( 0x0000000003FF0000 & val) << 38) >> 54) >> 2);
    out[24] = (int8_t)(((int64_t)(( 0x000000000000FFC0 & val) << 48) >> 54) >> 2);
    rest =          ( 0x000000000000003F & val) << 58; // 6 bits rest.
    // 5th:
    val = be64toh(*qword);
    //printf("0x%016lX\n",val);
    qword++;
    out[25] = (int8_t)(((int64_t)(((0xF000000000000000 & val) >> 6) | rest) >> 54) >> 2);
    out[26] = (int8_t)(((int64_t)(( 0x0FFC000000000000 & val) <<  4) >> 54) >> 2);
    out[27] = (int8_t)(((int64_t)(( 0x0003FF0000000000 & val) << 14) >> 54) >> 2);
    out[28] = (int8_t)(((int64_t)(( 0x000000FFC0000000 & val) << 24) >> 54) >> 2);
    out[29] = (int8_t)(((int64_t)(( 0x000000003FF00000 & val) << 34) >> 54) >> 2);
    out[30] = (int8_t)(((int64_t)(( 0x00000000000FFC00 & val) << 44) >> 54) >> 2);
    out[31] = (int8_t)(((int64_t)(( 0x00000000000003FF & val) << 54) >> 54) >> 2);
    rest = 0; // No rest.
    return qword;
}

void handle_packet_numbers_4096x10_s(char const *buf, char *out)
{   // Print 4096 numbers of 10 bit signed integers.
    uint64_t *qword0 = (uint64_t*)(buf);
    uint8_t* D = reinterpret_cast<uint8_t*>(out);
    for (int i = 0; i < 640 / 5; i++)
    {
        qword0 = unpack5(qword0, D);
        *D += 32;
    }
}

EDDPolnMerge10to8_1pol::EDDPolnMerge10to8_1pol(std::size_t nsamps_per_heap, std::size_t npol, int nthreads, DadaWriteClient& writer)
    : _nsamps_per_heap(nsamps_per_heap)
    , _npol(npol)
    , _nthreads(nthreads)
    , _writer(writer)
{
}

EDDPolnMerge10to8_1pol::~EDDPolnMerge10to8_1pol()
{
}

void EDDPolnMerge10to8_1pol::init(RawBytes& block)
{
    RawBytes& oblock = _writer.header_stream().next();
    if (block.used_bytes() > oblock.total_bytes())
    {
        _writer.header_stream().release();
        throw std::runtime_error("Output DADA buffer does not have enough space for header");
    }
    std::memcpy(oblock.ptr(), block.ptr(), block.used_bytes());
    char buffer[1024];
    ascii_header_get(block.ptr(), "SAMPLE_CLOCK_START", "%s", buffer);
    std::size_t sample_clock_start = std::strtoul(buffer, NULL, 0);
    ascii_header_get(block.ptr(), "CLOCK_SAMPLE", "%s", buffer);
    long double sample_clock = std::strtold(buffer, NULL);
    ascii_header_get(block.ptr(), "SYNC_TIME", "%s", buffer);
    long double sync_time = std::strtold(buffer, NULL);
    BOOST_LOG_TRIVIAL(debug) << "this is sample_clock_start " << sample_clock_start;
    BOOST_LOG_TRIVIAL(debug) << "this is sample_clock " << sample_clock;
    BOOST_LOG_TRIVIAL(debug) << "this is sync_time " << sync_time;
    BOOST_LOG_TRIVIAL(debug) << "this is sample_clock_start / sample_clock " << sample_clock_start / sample_clock;
    long double unix_time = sync_time + (sample_clock_start / sample_clock);
    char time_buffer[80];
    std::time_t unix_time_int;
    struct std::tm * timeinfo;
    double fractpart, intpart;
    fractpart = std::modf (static_cast<double>(unix_time) , &intpart);
    unix_time_int = static_cast<std::time_t>(intpart);
    timeinfo = std::gmtime (&unix_time_int);
    std::strftime(time_buffer, 80, "%Y-%m-%d-%H:%M:%S", timeinfo);
    std::stringstream utc_time_stamp;
    BOOST_LOG_TRIVIAL(debug) << "unix_time" << unix_time;
    BOOST_LOG_TRIVIAL(debug) << "fractional part " << fractpart;
    utc_time_stamp << time_buffer << "." << std::setw(10) << std::setfill('0') << std::size_t(fractpart * 10000000000) << std::setfill(' ');
    BOOST_LOG_TRIVIAL(debug) << "this is start time in utc " << utc_time_stamp.str().c_str() << "\n";
    //  std::cout << "this is sync_time MJD "<< mjd_time<< "\n";
    ascii_header_set(oblock.ptr(), "UTC_START", "%s", utc_time_stamp.str().c_str());
    ascii_header_set(oblock.ptr(), "UNIX_TIME", "%Lf", unix_time);
    oblock.used_bytes(oblock.total_bytes());
    _writer.header_stream().release();
    BOOST_LOG_TRIVIAL(info) << "Output header released" << "\n";
}

bool EDDPolnMerge10to8_1pol::operator()(RawBytes& block)
{
    std::cout << "Beginning of the operator" << std::endl;
    RawBytes& oblock = _writer.data_stream().next();

//    if (block.used_bytes() > oblock.total_bytes())
//        {
//        _writer.data_stream().release();
//        throw std::runtime_error("Output DADA buffer does not match with the input dada buffer");
//         }

    /* convert 10 bit to 8 bit data here */
    BOOST_LOG_TRIVIAL(debug) << "block.used_bytes() = " << block.used_bytes();
    BOOST_LOG_TRIVIAL(debug) << "Entering unpack loop";

    #pragma omp parallel for schedule(dynamic, _nthreads) num_threads(_nthreads)
    for (std::size_t kk = 0; kk < block.used_bytes() / HEAP_SIZE_10BIT ; ++kk)
    {
        char *buffer = block.ptr() + HEAP_SIZE_10BIT * kk;
        handle_packet_numbers_4096x10_s(buffer, oblock.ptr() + kk * _nsamps_per_heap );
    }
    oblock.used_bytes(block.used_bytes() * _nsamps_per_heap / HEAP_SIZE_10BIT);
    //oblock.used_bytes(block.used_bytes());
    _writer.data_stream().release();
    return false;
}
}//edd
}//effelsberg
}//psrdada_cpp
