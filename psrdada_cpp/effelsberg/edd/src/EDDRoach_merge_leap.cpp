#include "psrdada_cpp/effelsberg/edd/EDDRoach_merge_leap.hpp"
#include "ascii_header.h"
#include <immintrin.h>
#include <time.h>
#include <iomanip>
#include <cmath>

namespace psrdada_cpp {
namespace effelsberg {
namespace edd {

uint64_t interleave(uint32_t x, uint32_t y) {
	__m128i xvec = _mm_cvtsi32_si128(x);
	__m128i yvec = _mm_cvtsi32_si128(y);
	__m128i interleaved = _mm_unpacklo_epi8(yvec, xvec);
	return _mm_cvtsi128_si64(interleaved);
}

EDDRoach_merge_leap::EDDRoach_merge_leap(std::size_t nchunck, int nthreads, DadaWriteClient& writer)
	: _nchunck(nchunck)
	, _nthreads(nthreads)
	, _writer(writer)
{
}

EDDRoach_merge_leap::~EDDRoach_merge_leap()
{
}

void EDDRoach_merge_leap::init(RawBytes& block)
{
	RawBytes& oblock = _writer.header_stream().next();
	if (block.used_bytes() > oblock.total_bytes())
	{
		_writer.header_stream().release();
		throw std::runtime_error("Output DADA buffer does not have enough space for header");
	}
	std::memcpy(oblock.ptr(), block.ptr(), block.used_bytes());
    char buffer[1024];
    ascii_header_get(block.ptr(), "SAMPLE_CLOCK_START", "%s", buffer);
    std::size_t sample_clock_start = std::strtoul(buffer, NULL, 0);
    ascii_header_get(block.ptr(), "CLOCK_SAMPLE", "%s", buffer);
    long double sample_clock = std::strtold(buffer, NULL);
    ascii_header_get(block.ptr(), "SYNC_TIME", "%s", buffer);
    long double sync_time = std::strtold(buffer, NULL);
    long double unix_time = sync_time + (sample_clock_start / sample_clock);
    long double mjd_time = (unix_time / 86400.0 ) + 40587;
    std::ostringstream mjd_start;
    mjd_start << std::fixed;
    mjd_start << std::setprecision(12);
    mjd_start << mjd_time;
    ascii_header_set(oblock.ptr(), "MJD_START", "%s", mjd_start.str().c_str());
    ascii_header_set(oblock.ptr(), "UNIX_TIME", "%Lf", unix_time);
	oblock.used_bytes(oblock.total_bytes());
	_writer.header_stream().release();
}

bool EDDRoach_merge_leap::operator()(RawBytes& block)
{
	BOOST_LOG_TRIVIAL(info) << "nchucnk " << _nchunck << "\n";
	RawBytes& oblock = _writer.data_stream().next();
	std::size_t bytes_per_chunk = 4;
	std::size_t heap_size = HEAP_SIZE * _nchunck;
	std::size_t nbands =  _nchunck;
	#pragma omp parallel for num_threads(_nthreads)
	for (std::size_t xx = 0; xx < block.used_bytes() / heap_size; xx++)
	{
		std::vector<char*> ptrs(nbands);
		for (std::size_t ii = 0; ii < nbands; ++ii)
		{
			ptrs[ii] = block.ptr() + xx * heap_size + ii * heap_size / nbands;
		}
		const char *target = oblock.ptr() + xx * heap_size;

		for (std::size_t yy = 0; yy < heap_size / nbands / bytes_per_chunk; yy++)
		{
			for (std::size_t ii = 0; ii < nbands; ++ii)
			{
				std::memcpy((void*)target, (void*)ptrs[ii], bytes_per_chunk);
				ptrs[ii] += bytes_per_chunk;
				target += bytes_per_chunk;
			}
		}
	}
	oblock.used_bytes(block.used_bytes());
	_writer.data_stream().release();
	return false;
}
}//edd
}//effelsberg
}//psrdada_cpp

