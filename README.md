# psrdada-cpp

psrdada-cpp provides C++ wrappers for a subset for PSRDADA functionality (HDUs,
read/write clients, etc.) and data processing pipelines build with them.

The documentation of psrdadad-cpp can be found at: http://mpifr-bdg.pages.mpcdf.de/psrdada_cpp/

