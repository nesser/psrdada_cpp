=========================
PSRDADA-CPP Documentation
=========================

psrdada-cpp provides C++ wrappers for a subset for `PSRDADA
<http://psrdada.sourceforge.net/>` functionality (HDUs, read/write clients,
etc.) and data processing pipelines build with them.

Contents
========

.. toctree::
   :caption: API
   :maxdepth: 2

   _api/library_root

.. toctree::
   :caption: DEVEL
   :maxdepth: 1



